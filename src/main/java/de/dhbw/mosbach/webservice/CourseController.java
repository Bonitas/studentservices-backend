package de.dhbw.mosbach.webservice;

import de.dhbw.mosbach.entities.Course;
import de.dhbw.mosbach.repositories.CourseRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Set;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/course")
public class CourseController {

    @Inject
    private CourseRepository courseRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public Set<Course> getCourses() {
        return courseRepository.findAllCourses();
    }

    @RequestMapping(value = "/key", method = RequestMethod.GET, produces = "application/json")
    public Course getCourse(
            @RequestParam String key
    ) {
        return courseRepository.findByKey(key);
    }

    @RequestMapping(value = "/duration", method = RequestMethod.GET, produces = "application/json")
    public Set<Course> getWithLongestDuration(
            @RequestParam(required = false) String key
    ) {
        return courseRepository.findCourseWithLongestLectures(key == null ? "" : "%"+key+"%");
    }
}