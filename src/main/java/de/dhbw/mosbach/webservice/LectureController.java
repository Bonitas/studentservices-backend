package de.dhbw.mosbach.webservice;

import de.dhbw.mosbach.entities.Lecture;
import de.dhbw.mosbach.repositories.LectureRepository;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/lectures")
public class LectureController {

    @Inject
    private LectureRepository lectureRepository;

    @RequestMapping(path = "/", method = RequestMethod.GET, produces = "application/json")
    public Set<Lecture> getLecturesByCourse(
            @RequestParam String course
    ) {
        return lectureRepository.getUpcomingByCourse(course);
    }

    @RequestMapping(path = "/filter", method = RequestMethod.GET, produces = "application/json")
    public Set<Lecture> getLecturesByProfessor(
            @RequestParam(required = false) String course,
            @RequestParam String professor
    ) {
        Set<Lecture> lectures = lectureRepository.getUpcomingByProfessor(professor);
        if (!StringUtils.isEmpty(course)){
            // lectures = lectures.stream().filter(l -> l.getCourse() == course).collect(Collectors.toSet());
            lectures.removeIf(l -> !l.getCourse().equalsIgnoreCase(course));
        }
        return lectures;
        // http://localhost:8080/lectures/filter?course=wi15s&professor=C.Schalles
    }
}

