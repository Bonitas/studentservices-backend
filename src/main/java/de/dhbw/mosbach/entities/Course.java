package de.dhbw.mosbach.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.net.URL;

/**
 * Created by Waldemar on 21.02.2016.
 */
@Entity
public class Course implements Serializable {

    Course(){
        // used for JPA
    }

    public Course(String courseKey, URL calenderURL){
        this.courseKey = courseKey;
        this.calenderURL = calenderURL;
    }

    @Id
    private String courseKey;
    @Type(type = "org.hibernate.type.UrlType")
    private URL calenderURL;
    private Integer overallDuration;

    public String getCourseKey() {
        return courseKey;
    }

    public void setCourseKey(String courseKey) {
        this.courseKey = courseKey;
    }

    public URL getCalenderURL() {
        return calenderURL;
    }

    public void setCalenderURL(URL calenderURL) {
        this.calenderURL = calenderURL;
    }

    public Integer getOverallDuration() {
        return overallDuration;
    }

    public void setOverallDuration(Integer overallDuration) {
        this.overallDuration = overallDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return new EqualsBuilder()
                .append(this.courseKey, course.courseKey)
                .append(this.calenderURL, course.calenderURL)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 17)
                .append(courseKey)
                .append(calenderURL)
                .toHashCode();
    }

    @Override
    public String toString(){
        return new ToStringBuilder(this)
                .append("courseKey", courseKey)
                .append("calenderURL", calenderURL)
                .toString();
    }
}
