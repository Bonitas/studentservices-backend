package de.dhbw.mosbach.jobs;

import com.google.common.collect.Sets;
import de.dhbw.mosbach.GlobaleVars;
import de.dhbw.mosbach.entities.Course;
import de.dhbw.mosbach.repositories.CourseRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.StringReader;
import java.net.URL;
import java.util.Set;

/**
 * Created by Waldemar on 21.02.2016.
 */
@Component
public class CourseSync {

    @Inject
    CourseRepository courseRepository;
    @Inject
    LectureSync lectureSync;

    private boolean canSync = true;

    @Scheduled(cron = "0 0 7 ? * MON-FRI ")
    // @Scheduled(fixedRate = 1000000)
    public void loadCourses() {
        if (!isSyncAllowed())
            return;
        lectureSync.setCanSync(false);
        long startTime = System.currentTimeMillis();
        Set<Course> oldCourses = courseRepository.findAllCourses();
        RestTemplate restTemplate = new RestTemplate();
        String text = restTemplate.getForObject(GlobaleVars.URL_CALENDERS_LIST, String.class);
        Set<Course> courseSet = parseCourses(text);
        courseSet.removeAll(oldCourses);
        courseRepository.save(courseSet);
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.println("Synchronized courses in " + duration + " ms!");
        lectureSync.setCanSync(true);
    }

    private Set<Course> parseCourses(String text) {
        Set<Course> courseSet = Sets.newHashSet();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new StringReader(text));
            while ((line = reader.readLine()) != null) {
                String[] tmpLine = line.split(";");
                // the list contains wrong data -> clean it up
                if (!StringUtils.isEmpty(tmpLine[0]) && !tmpLine[0].equalsIgnoreCase("CALENDARS"))
                    courseSet.add(new Course(tmpLine[0], new URL(tmpLine[1])));
            }
        } catch (Exception e) {
            System.out.println("An error occurred while parsing courses!");
        }
        return courseSet;
    }

    public boolean isSyncAllowed() {
        return canSync;
    }

    public void setCanSync(boolean canSync) {
        this.canSync = canSync;
    }
}
