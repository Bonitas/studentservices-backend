package de.dhbw.mosbach.jobs;

import com.google.common.collect.Lists;
import de.dhbw.mosbach.entities.Course;
import de.dhbw.mosbach.repositories.CourseRepository;
import de.dhbw.mosbach.services.LectureService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by Waldemar on 21.02.2016.
 */
@Component
public class LectureSync {
    @Inject
    LectureService lectureService;
    @Inject
    private CourseRepository courseRepository;
    @Inject
    CourseSync courseSync;

    private boolean canSync = true;

    @Scheduled(cron = "0 0/10 9-17 * * MON-FRI")
    // @Scheduled(fixedRate = 10000000)
    public void synchronizeLectures() {
        if (!isSyncAllowed())
            return;
        System.out.println("Started lecture synchronization");
        long startTime = System.currentTimeMillis();
        courseSync.setCanSync(false);
        Collection<Future<String>> results = Lists.newArrayList();
        Set<Course> courses = courseRepository.findAllCourses();
        results.addAll(courses.stream().map(course -> lectureService.loadLectures(course)).collect(Collectors.toList()));

        // wait for all threads
        results.forEach(result -> {
            try {
                result.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        courseRepository.updateOverallDurations();
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.println("Finished lecture sync for " + results.size() + " lectures in " + duration + "ms");
        courseSync.setCanSync(true);
    }

    public boolean isSyncAllowed() {
        return canSync;
    }

    public void setCanSync(boolean canSync) {
        this.canSync = canSync;
    }
}
