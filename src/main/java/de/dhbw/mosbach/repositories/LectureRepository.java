package de.dhbw.mosbach.repositories;

import de.dhbw.mosbach.entities.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by Waldemar on 21.02.2016.
 */
public interface LectureRepository extends JpaRepository<Lecture, Integer> {
    @Query("select l from Lecture l where l.course = ?1")
    Set<Lecture> getByCourse(String course);

    @Query("select l from Lecture l where l.course = ?1 and l.endTime > current_date() order by l.endTime asc")
    Set<Lecture> getUpcomingByCourse(String course);

    @Query("select distinct l.location from Lecture l where (l.startTime between ?1 and ?2) or (l.endTime between ?1 and ?2) order by l.location asc")
    Set<String> getReservedRooms(LocalDateTime start, LocalDateTime end);

    // date check needed due to changed naming strategy
    @Query("select distinct l.location from Lecture l where l.startTime > '2016-01-01' order by l.location asc")
    Set<String> getAllRooms();

    @Query("select l from Lecture l where l.professor = ?1 and l.endTime > current_date() order by l.endTime asc")
    Set<Lecture> getUpcomingByProfessor(String professor);
}
