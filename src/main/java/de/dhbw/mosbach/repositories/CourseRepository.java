package de.dhbw.mosbach.repositories;

import de.dhbw.mosbach.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created by Waldemar on 21.02.2016.
 */
public interface CourseRepository extends JpaRepository<Course, String> {

    @Query("select c from Course c")
    Set<Course> findAllCourses();

    @Query("select c from Course c where c.courseKey = ?1")
    Course findByKey(String key);

    @Query(value = "SELECT * FROM Course c WHERE c.overallDuration = (SELECT MAX(cc.overallDuration) FROM Course cc WHERE cc.courseKey LIKE ?1) ORDER BY c.overallDuration desc", nativeQuery = true)
    Set<Course> findCourseWithLongestLectures(String key);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Course c SET c.overallDuration = (SELECT SUM(TIMESTAMPDIFF(MINUTE,l.startTime,l.endTime)) FROM Lecture l WHERE l.course = c.courseKey)", nativeQuery = true)
    void updateOverallDurations();
}
